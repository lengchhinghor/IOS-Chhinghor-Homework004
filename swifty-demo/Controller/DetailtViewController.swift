//
//  DetailtViewController.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit

class DetailtViewController: UIViewController {

    var article: Article?
    

    @IBOutlet weak var detailDesLbl: UITextView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailTitleLbl: UILabel!
    @IBOutlet weak var ImagePost: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        ImagePost.image = UIImage(named:article!.imageUrl);
        detailDesLbl.text = article?.description
        detailTitleLbl.text = article?.title
        
        let url = URL(string: article!.imageUrl ?? "")
        
        let defaultImage = UIImage(systemName: "camera.fill")
        
        self.ImagePost.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])
//        
        
//        print("Hello image",ImagePost!)

        // Do any additional setup after loading the view.
    }

}
